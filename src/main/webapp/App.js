
import React from 'react';
import ReactDOM from 'react-dom';


ReactDOM.render(
 
  <div>
        <button onClick={getCounter}>
          Get counter
        </button>
        
        <button onClick = {incrementCounter}>
          Increment counter
        </button>
      </div>, document.getElementById('root')
);

  
function getCounter(){
    var xhr = new XMLHttpRequest();

    xhr.addEventListener('load', () => {
    console.log(xhr.responseText)
    })
    xhr.open('GET', 'https://localhost:8080/get')
    xhr.send()
  }
  
  function incrementCounter(){
    var xhr = new XMLHttpRequest();

    xhr.addEventListener('load', () => {
    console.log(xhr.responseText)
    })
    xhr.open('POST', 'https://localhost:8080/register')
    xhr.send()
  }
  


