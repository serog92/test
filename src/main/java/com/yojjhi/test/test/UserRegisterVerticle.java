package com.yojjhi.test.test;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;

public class UserRegisterVerticle extends AbstractVerticle {

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    HttpServerOptions options = new HttpServerOptions();
    options.setHost("ec2-18-219-194-19.us-east-2.compute.amazonaws.com");
    options.setPort(8080);
    HttpServer server = vertx.createHttpServer(options);

    Router router = Router.router(vertx);
    router.route(HttpMethod.POST, "/register").handler(routingContext -> {
    });
    PgConnectOptions sqlConnectOptions = new PgConnectOptions();

    PoolOptions poolOptions = new PoolOptions()
      .setMaxSize(5);

    PgPool client = PgPool.pool(sqlConnectOptions, poolOptions);
    String query = "CREATE TABLE IF NOT EXISTS  users (id INTEGER PRIMARY KEY, counter INTEGER NOT NULL)";
    client.query(query);

    AtomicInteger res = new AtomicInteger();

    client.query("SELECT * FROM users").execute(fr -> {
      if (fr.succeeded()) {
        RowSet<Row> result = fr.result();
        Iterator<Row> it = result.iterator();
        while (it.hasNext()) {
          res.set(res.get() + 1 );
          it.next();
        }
      }
    });

    int updCounter = res.get() +1;
    String queryToUpdate = String.format("INSERT INTO users VALUES (%s, %s)", updCounter, updCounter);
    client.query(queryToUpdate).execute();
    client.close();
    server.requestHandler(router);
    startPromise.complete();
  }
}
