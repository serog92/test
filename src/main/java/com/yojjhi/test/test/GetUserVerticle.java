package com.yojjhi.test.test;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;

public class GetUserVerticle extends AbstractVerticle {

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    HttpServerOptions options = new HttpServerOptions();
    options.setHost("ec2-18-219-194-19.us-east-2.compute.amazonaws.com");
    options.setPort(8080);
    HttpServer server = vertx.createHttpServer();

    PgConnectOptions sqlConnectOptions = new PgConnectOptions();

    PoolOptions poolOptions = new PoolOptions()
      .setMaxSize(5);

    PgPool client = PgPool.pool(sqlConnectOptions, poolOptions);

    AtomicInteger res = new AtomicInteger();

    client.query("SELECT * FROM users").execute(fr -> {
      if (fr.succeeded()) {
        RowSet<Row> result = fr.result();
        Iterator<Row> it = result.iterator();
        while (it.hasNext()) {
          Row row = it.next();
          res.set(row.getInteger("counter"));
        }
      }
    });

    Router router = Router.router(vertx);
    client.close();

    router.route(HttpMethod.GET, "/get").handler(routingContext -> {

      HttpServerResponse response = routingContext.response();
      response
        .putHeader("content-type", "application/xml")
        .end(String.format("<h1>Hello counter is '%s' </h1>", res.get()));
    });


    server.requestHandler(router).listen(8080);
  }
}
